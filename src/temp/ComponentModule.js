import React, { useEffect } from 'react'
import { Route, Switch } from 'react-router-dom';
import './demo-css.css'
import Header from '../components/Header';
import Footer from '../components/Footer';
import PlanCard from '../components/PlanCard';
import PlanCard2 from '../components/PlanCard2';
import PlanCard3 from '../components/PlanCard3';
import SideNav from '../components/SideNav';
import FloatingMenu from '../components/FloatingMenu';
function ComponentModule(props) {
    useEffect(() => {
        
    }, [])
    return (
        <>
            <div className="left-panel">
                <a className="back" href="/">
                    <span className="icon icon-chevron-left"></span>
                    <span className="text"> back to Pages</span>    
                </a>
                <h1>Component List</h1>
                <ul>
                <li><a href={`${props.match.path}/header`}>Header</a></li>
                <li><a href={`${props.match.path}/footer`}>Footer</a></li>
                <li><a href={`${props.match.path}/PlanCard`}>PlanCard Type 1</a></li>
                <li><a href={`${props.match.path}/PlanCard2`}>PlanCard Type 2</a></li>
                <li><a href={`${props.match.path}/PlanCard3`}>PlanCard Type 3</a></li>
                <li><a href={`${props.match.path}/SideNav`}>Side Menu Bar</a></li>
                <li><a href={`${props.match.path}/FloatingMenu`}>Floating Menu</a></li>
                </ul>
            </div>
            <div className="right-panel">
                <div className="right-cont">
                    <Switch>
                        <Route path={props.match.path} exact component={Header} />
                        <Route path={`${props.match.path}/header`} exact component={Header} />
                        <Route path={`${props.match.path}/footer`} exact component={Footer} />
                        <Route path={`${props.match.path}/PlanCard`} exact component={PlanCard} />
                        <Route path={`${props.match.path}/PlanCard2`} exact component={PlanCard2} />
                        <Route path={`${props.match.path}/PlanCard3`} exact component={PlanCard3} />
                        <Route path={`${props.match.path}/SideNav`} exact component={SideNav} />
                        <Route path={`${props.match.path}/FloatingMenu`} exact component={FloatingMenu} />
                    </Switch>
                </div>
            </div>
        </>
    )
}

export default ComponentModule
