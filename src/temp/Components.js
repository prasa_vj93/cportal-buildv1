import React,{useState} from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import BannerAdvertise from '../components/BannerAdvertise'
import useSetBgImage from '../hooks/useSetBgImage'
import PlanCard from '../components/PlanCard'
import Toaster from '../components/Toaster'
import Loader from '../components/Loader'
import './demo-css.css'

function Components() {
    const [toasterState,setToasterState] = useState(false)
    const [loader,setloader] = useState(false)

    const showToasterHandler = ()=>{
        setToasterState(true);
        setTimeout(()=>{
            setToasterState(false);
        },3500)
    }
    const showLoaderHandler = ()=>{
        setloader(true)
        setTimeout(()=>{
            setloader(false);
        },3500)
    }
    useSetBgImage('.setBgSrc', '.getBgSrc')
    return (
        <>
            <div className="container container--refContainer">
                <h1 className="maintitle">Components</h1>  
                {/* <div className="compListItem">
                    <h2>Header</h2>
                    <Header />
                </div>
                <div className="compListItem">
                    <h2>Footer</h2>
                    <Footer/>
                </div> */}
                <div className="compListItem">
                    <h2>Banner </h2>
                    <BannerAdvertise/>
                </div>
                <div className="compListItem">
                    <h2>Card </h2>
                    <div style={{"width":"500px"}}>
                        <PlanCard/>
                    </div>
                </div>
                <div className="compListItem">
                    <h2>Toaster </h2>
                    <p>To show Toaster Click on bellow button</p>
                    <button className="btn" onClick={showToasterHandler}>Show Toaster</button>
                    {(toasterState)?
                    <Toaster toastType="Success!"  toastMessage="WhatsApp Optin can be modified through profile section anytime." vAlign="bottom" hAlign="center" />:null
                    }
                </div>
                <div className="compListItem">
                    <h2>Loader </h2>
                    <p>To show Loader Click on bellow button</p>
                    <button className="btn" onClick={showLoaderHandler}>Show Loader [for 3sec] </button>
                    
                    <Loader show={loader}/>
                    
                </div>
            </div>
        </>
    )
}

export default Components
