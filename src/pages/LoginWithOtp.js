import React, { useState,useEffect } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { NavLink } from "react-router-dom";
import Loader from "../components/Loader";

const LoginWithOtp = () => {
  const [loader,setLoader] = useState(true);
  const [tab, setTab] = useState("tab2");
  const [pwdShowState, setPwdShowState] = useState(false);

  useEffect(()=>{
    setLoader(false);
  },[])


  const setTabHandler = (e, param) => {
    e.preventDefault();
    switch (param) {
      case 'tab1':
        setTab('tab1');
        break;
      case 'tab2':
        setTab('tab2');
        break;
    }
  }
  const pwdShowStateHandler = () => {
    setPwdShowState(!pwdShowState);
  }
  return (
    <>
      <Header />
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Login</h1>
                <label className="loginJourney__lbl">Enter your information below to continue</label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="tab">
                    <ul className="tab__nav">
                      <li className={`tab__navBtn ${(tab === 'tab1') ? 'tab__navBtn--active' : ''}`}>
                        <NavLink to="#tab1" title="Login with password" onClick={(e) => { setTabHandler(e, 'tab1') }}>Login with password</NavLink>
                      </li>
                      <li className={`tab__navBtn ${(tab === 'tab2') ? 'tab__navBtn--active' : ''}`}>
                        <NavLink to="#tab2" title="Login with OTP" onClick={(e) => { setTabHandler(e, 'tab2') }}>Login with OTP</NavLink>
                      </li>
                    </ul>
                    <div className="tab__content">
                      <div id="tab1" className={`tab__pane ${(tab === 'tab1') ? 'tab__pane--active' : ''}`}>
                        <div className="inputGroup">
                          <label className="lbl">User ID</label>
                          <input
                            type="text"
                            className="formControl"
                            placeholder="Enter your User ID"
                          />
                          <div className="inputGroupAppend">
                            <NavLink to="" className="btn btn--link" title="Forgot User ID?">Forgot User ID?</NavLink>
                          </div>
                          <span className="error">
                            We did not find this ID in our database. Please
                            check User ID and enter again
                                </span>
                        </div>
                        <div className="inputGroup inputGroup--pass">
                          <label className="lbl">Password</label>
                          <div className="inputGroup__addon">
                            <input
                              type={`${(pwdShowState) ? 'text' : 'password'}`}
                              className="formControl"
                              placeholder="Enter Password"
                            />

                            <button className="btn" onClick={pwdShowStateHandler} type="button">
                              <span className={`icon ${(pwdShowState) ? 'icon-no-view' : 'icon-view'}`}></span>
                            </button>
                          </div>
                          <div className="inputGroupAppend">
                            <NavLink to="" className="btn btn--link" title="Forgot password?">Forgot password?</NavLink>
                          </div>
                          <span className="error">
                            Your password in incorrect. Please try again.
                                </span>
                        </div>
                        <div className="lytLogin__actBtn actBtn">
                          <button className="btn btn--link" type="button">
                            Register now
                                </button>
                          <button className="btn" type="button">
                            Submit
                                </button>
                        </div>
                      </div>
                      <div id="tab2" className={`tab__pane ${(tab === 'tab2') ? 'tab__pane--active' : ''}`}>
                        <div className="inputGroup">
                          <label className="lbl">One ABC ID</label>
                          <input
                            type="text"
                            className="formControl"
                            placeholder="Enter your User ID"
                          />
                          <div className="inputGroupAppend">
                            <NavLink to="" className="btn btn--link" title="Forgot User ID?">Forgot User ID?</NavLink>
                          </div>
                          <span className="error">
                            We did not find this ID in our database. Please
                            check User ID and enter again
                                </span>
                        </div>
                        <div className="lytLogin__actBtn actBtn">
                          <button className="btn" type="button">
                            Send OTP
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="lytLogin__rhs">
            <div className="creatId">
              <div className="creatId__decs">
                <h3 className="creatId__title">
                  Existing customer but don't have an Aditya Birla Capital One
                  ID?
                </h3>
                <label className="info creatId__info">
                  <span className="icon icon-info"></span>
                  <span className="info__text">Create your One ABC ID</span>
                </label>
              </div>
              <div className="creatId__act-wrap">
                <button type="button" className="btn btn--primary btn--white">
                  Create Your ID
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
      <Loader show={loader} />
    </>
  );
};

export default LoginWithOtp;
