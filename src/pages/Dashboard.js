import React, { useEffect, useState,useRef } from "react";
import Swiper from "swiper";
import { isMobile } from "react-device-detect";
import Header from "../components/Header";
import Footer from "../components/Footer";
import SideNav from "../components/SideNav";
import PlanCard from "../components/PlanCard";
import PlanCard2 from "../components/PlanCard2";
import PlanCard3 from "../components/PlanCard3";
import FloatingMenu from "../components/FloatingMenu";
import Loader from "../components/Loader";
import useSetBgImage from "../hooks/useSetBgImage";
import BannerAdvertise from "../components/BannerAdvertise";
import { NavLink } from "react-router-dom";



function Dashboard() {
  const node = useRef();
  const [loader,setLoader] = useState(true);
  const [className, setClassName] = useState("");
  const [notificationInfoState, setNotificationInfoState] = useState(false);
  const [covidState, setCovidState] = useState(true);
  const [sidebarMobileState, setSidebarMobileState] = useState(false);
  const [playVideoState, setPlayVideoState] = useState(false);
  const [whatsappModalState, setWhatsappModalState] = useState(false);

  let mIconTextswiper;
  let bsPlanCardSwiper;

  const handleClickOutside = (e)=>{
    if (node.current.contains(e.target)) {
      // inside click
      return;
    }
    setNotificationInfoState(false);
    // outside click 
  }
  useEffect(() => {
    // add when mounted
    document.addEventListener("click", handleClickOutside);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);
  useEffect(() => {
    setLoader(false);
    
    mIconTextswiper = new Swiper(".js-icontext-swiper", {
      speed: 400,
      slidesPerView: "auto",
      breakpoints: {
        480: {
          slidesPerView: 6,
        },
      },
    });
    bsPlanCardSwiper = new Swiper(".js-planCard-swiper", {
      speed: 400,
      slidesPerView: "auto",
      breakpoints: {
        480: {
          slidesPerView: 2,
        },
      },
      navigation: {
        nextEl: ".js-planCard-swiper .swiper-button-next",
        prevEl: ".js-planCard-swiper .swiper-button-prev",
      },
    });
  });
  const handleLeftMenuDock = () => {
    const position = window.pageYOffset;
    if (position >= 162) setClassName("sideNavOuter--fixed");
    else setClassName("");
  };
  useEffect(() => {
    window.addEventListener("scroll", handleLeftMenuDock, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleLeftMenuDock);
    };
  }, []);
  const covidCloseHandler = () => {
    setCovidState(false);
  };
  const notificationStateHandler = (e) => {
    e.preventDefault();
    
      setNotificationInfoState(!notificationInfoState);
    
  };
  const sideBarHandlerMobile = () => {
    if (sidebarMobileState) {
      setSidebarMobileState(false);
    } else {
      setSidebarMobileState(true);
    }
  };
  const openVideoModalHandler = (e, param) => {
    e.preventDefault();
    if (param === "open") {
      setPlayVideoState(true);
    } else {
      setPlayVideoState(false);
    }
  };
  const whatsappModalStateHandler = (e, param) => {
    e.preventDefault();
    if (param === "open") {
      setWhatsappModalState(true);
    } else {
      setWhatsappModalState(false);
    }
  };
  useSetBgImage('.setBgSrc', '.getBgSrc')
  return (
    <>
      <Header />
      
      <div className="container container--refContainer">
        <SideNav docClass={className} openStateMobile={sidebarMobileState} />
        <div className="contentWrapper">
          {/* {start content} */}
          <div className="actionHeader">
            {isMobile ? (
              <h1 className="actionHeader__title" onClick={sideBarHandlerMobile}>
                Dashboard
                <span className="icon icon-chevron-down"></span>
              </h1>
            ) : (
                <h1 className="actionHeader__title">
                  Dashboard
                  <span className="icon icon-chevron-down"></span>
                </h1>
              )}
            <ul className="iconList">
              <li className="iconList__item">
                <NavLink to="" className="icon icon-search" title="search icon"></NavLink>
              </li>
              <li className="iconList__item">
                <NavLink to="" title="what's app icon" onClick={(e) => { whatsappModalStateHandler(e, "open"); }} className="icon icon-whats-app"></NavLink>
              </li>
              <li className="iconList__item desktop">
                <NavLink to="" className="icon icon-call" title="call icon"></NavLink>
              </li>
              <li className="iconList__item desktop">
                <NavLink to="" className="icon icon-branch-locator" title="branch locator icon"></NavLink>
              </li>
              <li ref={node} className="iconList__item">
                <NavLink to="" title="notification icon" className="icon icon-notification" onClick={(e) => { notificationStateHandler(e); }} ></NavLink>
                <div className={`iconList__submenu ${notificationInfoState ? "iconList--active" : ""}`}>
                  <div  className="notificationInfo">
                    <button  className="notificationInfo__cancel btn btn--icon" onClick={(e) => { notificationStateHandler(e); }}>
                      <span className="icon icon-cancel"></span>
                    </button>
                    <div  className="notificationInfo__cont">
                      <h3 className="notificationInfo__title">
                        Your last transaction
                      </h3>
                      <ul className="notificationInfo__list">
                        <li className="notificationInfo__item">
                          <NavLink to="" title="New address details verified for Term Insurance Policy No. 124346983" >
                            New address details verified for <strong>Term Insurance Policy No. 124346983</strong>
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink to="" title="Request processing. Change should reflect within 04 to 05 working days" >
                            Request processing. Change should reflect within 04 to 05 working days
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink to="" title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983">
                            Please add Purpose of Insurance for your <strong> Term Insurance Policy No. 1234346983</strong>. This will help us serve you better in the future.
                          </NavLink>
                          <NavLink to="" title="Add now" className="notificationInfo__paynow btn btn--link">
                            Add now
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink to="" title="New address details verified for Term Insurance Policy No. 124346983">
                            New address details verified for{" "}
                            <strong>Term Insurance Policy No. 124346983</strong>
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Request processing. Change should reflect within 04 to 05 working days"
                          >
                            Request processing. Change should reflect within 04
                            to 05 working days
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983"
                          >
                            Please add Purpose of Insurance for your{" "}
                            <strong>
                              Term Insurance Policy No. 1234346983
                            </strong>
                            . This will help us serve you better in the future.
                          </NavLink>
                          <NavLink
                            to=""
                            title="Add now"
                            className="notificationInfo__paynow btn btn--link"
                          >
                            Add now
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="New address details verified for Term Insurance Policy No. 124346983"
                          >
                            New address details verified for{" "}
                            <strong>Term Insurance Policy No. 124346983</strong>
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Request processing. Change should reflect within 04 to 05 working days"
                          >
                            Request processing. Change should reflect within 04
                            to 05 working days
                          </NavLink>
                        </li>
                        <li className="notificationInfo__item">
                          <NavLink
                            to=""
                            title="Please add Purpose of Insurance for your Term Insurance Policy No. 1234346983"
                          >
                            Please add Purpose of Insurance for your{" "}
                            <strong>
                              Term Insurance Policy No. 1234346983
                            </strong>
                            . This will help us serve you better in the future.
                          </NavLink>
                          <NavLink
                            to=""
                            title="Add now"
                            className="notificationInfo__paynow btn btn--link"
                          >
                            Add now
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                    <NavLink
                      to=""
                      title="View all"
                      className="notificationInfo__viewall btn btn-primary"
                    >
                      View all
                    </NavLink>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <ol className="breadcrumb desktop">
            <li className="breadcrumb__item">
              <NavLink to="" className="breadcrumb__link" title="Home">
                Home<span className="icon icon-chevron-right"></span>
              </NavLink>
            </li>
            <li className="breadcrumb__item">Dashboard</li>
          </ol>
          <div className="section">
            <div className="section__head">
              <h3 className="section__title">Welcome Kabir Roy</h3>
            </div>
            <div className="section__cont">
              <div className="swiper-container js-icontext-swiper">
                <div className="swiper-wrapper">
                  <div className="swiper-slide">
                    <NavLink
                      to=""
                      className="icontext icontext--highlight"
                      title="Upgrade"
                    >
                      <span className="icontext__icon icon icon-upgrade"></span>
                      <span className="icontext__text">Upgrade</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="" className="icontext" title="Pay Premium">
                      <span className="icontext__icon icon icon-pay"></span>
                      <span className="icontext__text">Pay Premium</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink
                      to=""
                      className="icontext"
                      title="Download Tax Certificate"
                    >
                      <span className="icontext__icon icon icon-download-certificate"></span>
                      <span className="icontext__text">
                        Download Tax Certificate
                      </span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink
                      to=""
                      className="icontext"
                      title="Set Standing Instructions"
                    >
                      <span className="icontext__icon icon icon-instructions"></span>
                      <span className="icontext__text">
                        Set Standing Instructions
                      </span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="" className="icontext" title="Set Alerts">
                      <span className="icontext__icon icon icon-time"></span>
                      <span className="icontext__text">Set Alerts</span>
                    </NavLink>
                  </div>
                  <div className="swiper-slide">
                    <NavLink to="" className="icontext" title="Service Requests">
                      <span className="icontext__icon icon icon-service-request"></span>
                      <span className="icontext__text">Service Requests</span>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {covidState ? (
            <div className="covid">
              <div className="covid__desc">
                <h3 className="covid__title">COVID 19</h3>
                <h4 className="covid__subtitle">
                  Our branches are currently closed as per government
                  guidelines. We regret the inconvenience.
                </h4>
                <div className="actBtn covid__actBtn">
                  <NavLink to="" className="btn btn--link" title="Email us">
                    Email us
                  </NavLink>
                  <NavLink
                    to=""
                    className="btn btn--link"
                    title="Connect on Whatsapp"
                  >
                    Connect on Whatsapp
                  </NavLink>
                </div>
              </div>
              <span className="covid__info icon icon-info"></span>
              <button
                onClick={covidCloseHandler}
                className="covid__cancel btn btn--icon"
              >
                <span className="icon icon-cancel"></span>
              </button>
            </div>
          ) : null}
          <div className="section">
            <div className="section__head">
              <h3 className="section__title">
                Your plan for a secure future is in great shape!
              </h3>
              <h4 className="section__subtitle">
                Here’s what your portfolio looks like
              </h4>
            </div>
            <div className="section__cont">
              <div className="swiper-container js-planCard-swiper">
                <div className="swiper-wrapper">
                  <div className="swiper-slide">
                    <PlanCard />
                  </div>
                  <div className="swiper-slide">
                    <PlanCard2 />
                  </div>
                  <div className="swiper-slide">
                    <PlanCard3 />
                  </div>
                  <div className="swiper-slide">
                    <PlanCard />
                  </div>
                  <div className="swiper-slide">
                    <PlanCard2 />
                  </div>
                  <div className="swiper-slide">
                    <PlanCard3 />
                  </div>
                </div>
                <div className="swiper-button-prev">
                  <span className="icon icon-chevron-left"></span>
                </div>
                <div className="swiper-button-next">
                  <span className="icon icon-chevron-right"></span>
                </div>
              </div>
            </div>
          </div>
          {!isMobile ? (
            <div className="section">
              <div className="section__cont">
                <div className="video setBgSrc">
                  <span className="video__poster">
                    <img className="getBgSrc" src="/assets/images/video-poster.jpg" title="video poster" alt="video poster" />
                  </span>
                  <button
                    onClick={(e) => {
                      openVideoModalHandler(e, "open");
                    }}
                    className="video__icon icon icon-video"
                  ></button>
                </div>
              </div>
            </div>
          ) : null}
          <div className="section">
            <div className="section__cont">
              <BannerAdvertise/>
              <div className="whtspAcc">
                <div className="whtspAcc__lhs">
                  <h3 className="whtspAcc__title">
                    Manage your account on WhatsApp
                  </h3>
                </div>
                <div className="whtspAcc__rhs">
                  <h4 className="whtspAcc__subtitle">
                    Lorem Ipsum is simply dummy text.
                  </h4>
                  <div className="whtspAcc__ctrl formGroup">
                    <span className="whtspAcc__icon icon icon-whats-app"></span>
                    <input
                      type="text"
                      className="whtspAcc__input formControl" value="9988776565" readOnly
                    />
                    <button className="whtspAcc__btn btn btn--icon">
                      <span className="icon icon-arrow-right"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {isMobile ? (
            <div className="section">
              <div className="section__cont">
                <div className="video setBgSrc">
                  <span className="video__poster">
                    <img className="getBgSrc"
                      src="/assets/images/video-poster.jpg"
                      title="Video Poster"
                      alt="Video Poster"
                    />
                  </span>
                  <button
                    onClick={(e) => {
                      openVideoModalHandler(e, "open");
                    }}
                    className="video__icon icon icon-video"
                  ></button>
                </div>
              </div>
            </div>
          ) : null}
          <div className="section">
            <div className="section__head">
              <h3 className="section__title">Upcoming Activity</h3>
            </div>
            <div className="section__cont">
              <dl className="activityList">
                <dt className="activityList__title">ABSLI Health Plan</dt>
                <dd className="activityList__desc">
                  <span className="activityList__desctext">
                    Premium due - 1 April 2020
                  </span>
                  <NavLink to="" className="btn btn--link" title="Pay now">
                    Pay now
                  </NavLink>
                </dd>
                <dt className="activityList__title">ABSLI Annuity Plan</dt>
                <dd className="activityList__desc">
                  <span className="activityList__desctext">
                    Initiated claim - 6 April 2020
                  </span>
                  <NavLink to="" className="btn btn--link" title="Track claim">
                    Track claim
                  </NavLink>
                </dd>
                <dt className="activityList__title">ABSLI Par Plan</dt>
                <dd className="activityList__desc">
                  <span className="activityList__desctext">
                    Loan approved - 12 February 2020
                  </span>
                  <NavLink
                    to=""
                    className="btn btn--link"
                    title="Track loan application"
                  >
                    Track loan application
                  </NavLink>
                </dd>
              </dl>
            </div>
          </div>
          {/* {end content} */}
        </div>
      </div>
      <Footer />
      {playVideoState ? (
        <>
          <div className={`modal ${playVideoState ? "modal--active" : ""}`}>
            <div className="modal__dialog modal--typ-md">
              <div className="modal__content">
                <div className="modal__header">
                  <button
                    type="button"
                    className="btn btn--icon"
                    onClick={(e) => {
                      openVideoModalHandler(e, "");
                    }}
                  >
                    <span className="icon icon-cancel"></span>
                  </button>
                </div>
                <div className="modal__body">
                  <iframe
                    width="100%"
                    height="478"
                    src="https://www.youtube.com/embed/i3vhcvSuXmQ"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
          <span className={`overlay ${playVideoState ? "active" : ""}`}></span>
        </>
      ) : null}

      <div className={`modal ${whatsappModalState ? "modal--active" : ""}`}>
        <div className="modal__dialog">
          <div className="modal__content">
            <div className="modal__header">
              <h5 className="modal__title">Welcome back Kabir Roy</h5>
              <h5 className="modal__subtitle">
                Help us to know you better by providing following details
              </h5>
              <button
                type="button"
                onClick={(e) => {
                  whatsappModalStateHandler(e, "");
                }}
                className="btn btn--icon"
              >
                <span className="icon icon-cancel"></span>
              </button>
            </div>
            <div className="modal__body">
              <div className="modal--whatsapp">
                <div className="checkbox">
                  <input type="checkbox" className="checkbox__input" id="a" />
                  <label className="checkbox-lbl" htmlFor="a">
                    WhatsApp Opt-in
                  </label>
                </div>
                <p>
                  Enabling WhatsApp Opt-in will allow you to receive important
                  information & updates over WhatsApp
                </p>
                <span className="note">
                  <span className="note__text">Your registered mobile number</span>
                  <span className="note__num">98300 98330</span>
                </span>
              </div>
            </div>
            <div className="actBtn">
              <button
                type="button"
                onClick={(e) => {
                  whatsappModalStateHandler(e, "");
                }}
                className="btn btn--link"
              >
                Skip this for now
              </button>
              <button type="button" className="btn">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
      <span className={`overlay ${whatsappModalState ? "active" : ""}`}></span>

      
      <FloatingMenu />
      <Loader show={loader} />
      
    </>
  );
}

export default Dashboard;
