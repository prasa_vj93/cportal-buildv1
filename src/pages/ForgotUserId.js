import React, { useState,useEffect } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import DateFnsUtils from "@date-io/date-fns";
import Loader from "../components/Loader";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  DatePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { NavLink } from "react-router-dom";

const ForgotUserId = () => {
  const [loader,setLoader] = useState(true);
  const [selectedDate, setSelectedDate] = useState(null);
  const [ABCidModalState,setABCidModalState] = useState(true)

  useEffect(()=>{
    setLoader(false);
  },[])

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  const closeModalHandler = ()=>{
    setABCidModalState(false);
  }
  return (
    <>
      <Header />
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Forgot User ID</h1>
                <label className="loginJourney__lbl">
                  Enter your information below to continue
                </label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="formGroup typ-error">
                    <label className="lbl">Policy No./Application No.</label>
                    <input
                      type="text"
                      className="formControl"
                      placeholder="Enter hrer"
                    />
                    <span className="error">
                      Please check the Policy No./Application No. and try again
                    </span>
                  </div>
                  <div className="formGroup">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        autoOk
                        disableFuture
                        inputVariant="standard"
                        openTo="year"
                        format="dd/MM/yyyy"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        label="Policy Owner Date of Birth (DD/MM/YYYY)"
                        placeholder="DD/MM/YYYY"
                        views={["year", "month", "date"]}
                        value={selectedDate}
                        onChange={handleDateChange}
                        keyboardIcon={''}
                        helperText={''}
                      />
                    </MuiPickersUtilsProvider>
                    <span className="error">
                      Please check the Policy No./Application No. and try again
                    </span>
                  </div>
                  <div className="lytLogin__actBtn actBtn">
                    <button className="btn" type="button">
                      Send OTP
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="lytLogin__rhs">
            <div className="creatId">
              <div className="creatId__decs">
                <h3 className="creatId__title">
                  Existing customer but don't have an Aditya Birla Capital One
                  ID?
                </h3>
                <label className="info creatId__info">
                  <span className="icon icon-info"></span>
                  <span className="info__text">Create your One ABC ID</span>
                </label>
              </div>
              <div className="creatId__act-wrap">
                <button type="button" className="btn btn--primary btn--white">
                  Create Your ID
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`modal ${(ABCidModalState)?'modal--active':''}`}>
        <div className="modal__dialog">
          <div className="modal__content">
            <div className="modal__header">
              <h5 className="modal__title">ABC One ID sent</h5>
              <button type="button" className="btn btn--icon" onClick={closeModalHandler}>
                <span className="icon icon-cancel"></span>
              </button>
            </div>
            <div className="modal__body">
              <p className="modal__para">Your ABC One ID has been sent to your registered mobile or email ID</p>
            </div>
            <div className="actBtn">
              <button type="button" className="btn">Go to Login</button>
            </div>
          </div>
        </div>
      </div>
      <span className={`overlay ${(ABCidModalState)?'active':''}`}></span>
      <Footer />
      <Loader show={loader} />
    </>
  );
};

export default ForgotUserId;
