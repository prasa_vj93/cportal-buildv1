import React,{useEffect,useState} from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import { NavLink } from 'react-router-dom';
import Loader from "../components/Loader";

const RegisterOtp = () => {
    const [loader,setLoader] = useState(true);

    useEffect(()=>{
        setLoader(false);
      },[])


    return (
    <>
        <Header/>
            <div className="container">
                <div className="lytLogin">
                    <div className="lytLogin__lhs">
                    <NavLink className="btnBack" to="" title="Go Back">
                    <span className="icon icon-arrow-left"></span>
                    <span className="text">Go Back</span>
                    </NavLink>
                    <div className="loginJourney">
                    <div className="loginJourney__head">
                        <h1 className="loginJourney__title">Enter OTP</h1>
                        <p className="loginJourney__para">We have sent an OTP to the registered mobile number <strong>98******78</strong> &amp; email ID <strong>ka****oy@g***com.</strong> Please enter it below to complete verification.</p>
                    </div>
                    <div className="loginJourney__cont card">
                        <form>
                            <div className="inputGroup inputGroup--otp inputGroup--error">
                                <label className="lbl">Enter OTP</label>
                                <input type="text" className="formControl mobile" placeholder="Enter OTP"/>
                                <div className="inputGroup__otpBox desktop">
                                    <input type="text" className="formControl" />
                                    <input type="text" className="formControl" />
                                    <input type="text" className="formControl" />
                                    <input type="text" className="formControl" />
                                    <input type="text" className="formControl" />
                                    <input type="text" className="formControl" />
                                </div>

                                <div className="inputGroupAppend">
                                    <span className="inputGroupText">
                                        <NavLink className="btn btn--link disabled" to="" title="Resend OTP">Resend OTP</NavLink>
                                        <span className="count">00:59</span>
                                    </span>
                                </div>
                                <span className="error">Please enter correct OTP and try again</span>
                            </div>
                            <div className="lytLogin__actBtn actBtn">
                                <button className="btn disabled" type="button">Verify</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
                    <div className="lytLogin__rhs">
                    <div className="creatId">
                    <div className="creatId__decs">
                        <h3 className="creatId__title">Existing customer but don't have an Aditya Birla Capital One ID?</h3>
                        <label className="info creatId__info">
                        <span className="icon icon-info"></span>
                        <span className="info__text">Create your One ABC ID</span>
                        </label>
                    </div>
                    <div className="creatId__act-wrap">
                        <button type="button" className="btn btn--primary btn--white">
                        Create Your ID
                        </button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        <Footer/>
        <Loader show={loader} />
    </>
    )
}

export default RegisterOtp;
