import React,{useState,useEffect} from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { NavLink } from "react-router-dom";
import Loader from "../components/Loader";

const LoginUserId = () => {
  const [loader,setLoader] = useState(true);
  const [loginModalState,setLoginModalState] = useState(true);
  const [pwdShowState,setPwdShowState] = useState(false);

  useEffect(()=>{
    setLoader(false);
  },[])

  const closeLoginModal = ()=>{
    setLoginModalState(false);
  }
  const pwdShowStateHandler = ()=>{
    setPwdShowState(!pwdShowState);
  }
  return (
    <>
      <Header />
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Login</h1>
                <label className="loginJourney__lbl">
                  Enter your information below to continue
                </label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="inputGroup">
                    <label className="lbl">User ID</label>
                    <input
                      type="text"
                      className="formControl"
                      placeholder="Enter your User ID"
                    />
                    <div className="inputGroupAppend">
                      <NavLink className="btn btn--link" to="" title="Forgot User ID?">Forgot User ID?</NavLink>
                    </div>
                    <span className="error">
                      We did not find this ID in our database. Please check User
                      ID and enter again
                    </span>
                  </div>
                  <div className="lytLogin__actBtn actBtn">
                    <button className="btn btn--link" type="button">
                      Register now
                    </button>
                    <button className="btn" type="button">
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="lytLogin__rhs">
            <div className="creatId">
              <div className="creatId__decs">
                <h3 className="creatId__title">
                  Existing customer but don't have an Aditya Birla Capital One
                  ID?
                </h3>
                <label className="info creatId__info">
                  <span className="icon icon-info"></span>
                  <span className="info__text">Create your One ABC ID</span>
                </label>
              </div>
              <div className="creatId__act-wrap">
                <button type="button" className="btn btn--primary btn--white">
                  Create Your ID
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`modal ${(loginModalState)?'modal--active':''}`}>
        <div className="modal__dialog">
          <div className="modal__content">
            <div className="modal__header">
              <h5 className="modal__title">You have an existing ABSLI ID</h5>
              <button type="button" onClick={closeLoginModal} className="btn btn--icon">
                <span className="icon icon-cancel"></span>
              </button>
            </div>
            <div className="modal__body">
              <p className="modal__para">
                We have migrated to a new portal to provide you with an enhanced
                experience across the entire Aditya Birla Capital portfolio.
                Please register for one ABC ID to experience it.
              </p>
              <div className="modal__highlight">
                <p>
                  We have migrated to a new portal to provide you with an
                  enhanced experience across the entire Aditya Birla Capital
                  portfolio. Please register for one ABC ID to experience it.
                </p>
              </div>
            </div>
            <div className="actBtn actBtn--col2">
              <button type="button" className="btn btn--outline">
                Continue with existing ID
              </button>
              <button type="button" className="btn">
                Register for One ABC ID
              </button>
            </div>
          </div>
        </div>
      </div>
      <span className={`overlay ${(loginModalState)?'active':''}`}></span>
      <Footer />
      <Loader show={loader} />
    </>
  );
};

export default LoginUserId;
