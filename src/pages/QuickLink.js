import React,{useState,useEffect} from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  DatePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { NavLink } from "react-router-dom";
import Loader from "../components/Loader";

const QuickLink = () => {
  const [loader,setLoader] = useState(true);
  const [selectedDate, setSelectedDate] = useState(null);

  useEffect(()=>{
    setLoader(false);
  },[])

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  return (
    <>
      <Header />
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Update Nominee</h1>
                <label className="loginJourney__lbl">Enter your information below to continue</label>
              </div>
              <div className="loginJourney__cont card">
                <label className="note">NRI customer can login with Email ID or Policy No. only</label>
                <form>
                  <div className="formGroup formGroup--error">
                    <label className="lbl">Registered Email ID/ Mobile No./ Policy No.</label>
                    <input
                      type="text"
                      className="formControl"
                      placeholder="Enter hrer"
                    />
                    <span className="error">We did not find this email id in our database</span>
                  </div>
                  <div className="formGroup">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        autoOk
                        disableFuture
                        inputVariant="standard"
                        openTo="year"
                        format="dd/MM/yyyy"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        label="Policy Owner Date of Birth (DD/MM/YYYY)"
                        placeholder="DD/MM/YYYY"
                        views={["year", "month", "date"]}
                        value={selectedDate}
                        onChange={handleDateChange}
                        keyboardIcon={''}
                        helperText={''}
                      />
                    </MuiPickersUtilsProvider>
                    <span className="error">
                      Please check the Policy No./Application No. and try again
                    </span>
                  </div>
                  <div className="lytLogin__actBtn actBtn">
                    <button className="btn" type="button">
                      Send OTP
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="lytLogin__rhs">
            <div className="creatId">
              <div className="creatId__decs">
                <h3 className="creatId__title">
                  Existing customer but don't have an Aditya Birla Capital One
                  ID?
                </h3>
                <label className="info creatId__info">
                  <span className="icon icon-info"></span>
                  <span className="info__text">Create your One ABC ID</span>
                </label>
              </div>
              <div className="creatId__act-wrap">
                <button type="button" className="btn btn--primary btn--white">
                  Create Your ID
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
      <Loader show={loader} />
    </>
  );
};

export default QuickLink;
