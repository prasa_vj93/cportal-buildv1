import React,{useState,useEffect} from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { NavLink } from "react-router-dom";
import Loader from "../components/Loader";

const Login = () => {
  const [loader,setLoader] = useState(true);
  const [pwdShowState,setPwdShowState] = useState(false);

  useEffect(()=>{
    setLoader(false);
  },[])

  const pwdShowStateHandler = ()=>{
    setPwdShowState(!pwdShowState);
  }
  return (
    <>
      <Header />
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Login</h1>
                <label className="loginJourney__lbl">
                  Enter your information below to continue
                </label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="inputGroup">
                    <label className="lbl">User ID</label>
                    <input
                      type="text"
                      className="formControl"
                      placeholder="Enter your User ID"
                    />
                    <div className="inputGroupAppend">
                    </div>
                    <span className="error">
                      We did not find this ID in our database. Please check User
                      ID and enter again
                    </span>
                  </div>
                  <div className="inputGroup inputGroup--pass">
                    <label className="lbl">Password</label>
                    <div className="inputGroup__addon">
                      <input
                        type={`${(pwdShowState)?'text':'password'}`}
                        className="formControl"
                        placeholder="Enter Password"
                      />
                      <button className="btn" onClick={pwdShowStateHandler} type="button">
                        <span className={`icon ${(pwdShowState)?'icon-no-view':'icon-view'}`}></span>
                      </button>
                    </div>
                    <div className="inputGroupAppend">
                      <NavLink className="btn btn--link" to="" title="Forgot password?">Forgot password?</NavLink>
                    </div>
                    <span className="error">
                      Your password in incorrect. Please try again.
                    </span>
                  </div>
                  <div className="lytLogin__actBtn actBtn">
                    <button className="btn btn--link" type="button">
                      Register now
                    </button>
                    <button className="btn" type="button">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="lytLogin__rhs">
            <div className="creatId">
              <div className="creatId__decs">
                <h3 className="creatId__title">
                  Existing customer but don't have an Aditya Birla Capital One
                  ID?
                </h3>
                <label className="info creatId__info">
                  <span className="icon icon-info"></span>
                  <span className="info__text">Create your One ABC ID</span>
                </label>
              </div>
              <div className="creatId__act-wrap">
                <button type="button" className="btn btn--primary btn--white">
                  Create Your ID
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
      <Loader show={loader} />
    </>
  );
};

export default Login;
