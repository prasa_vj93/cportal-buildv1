import {useEffect} from 'react'

function useSetBgImage(parentObjClass,imageClass) {
    useEffect(()=>{
        let imgArr = document.querySelectorAll(`${imageClass}`);
        let parentObjArr = document.querySelectorAll(`${parentObjClass}`);
        
        imgArr.forEach((item,index)=>{
           
            let styles = {
                'background-image':`url(${item.currentSrc})`
            }

            Object.assign(parentObjArr[index].style,styles)
            item.style.display='none';
        })
        window.addEventListener("resize", ()=>{
            imgArr.forEach((item,index)=>{
                let styles = {
                    'background-image':`url(${item.currentSrc})`
                }
    
                Object.assign(parentObjArr[index].style,styles)

            })
        });
    })
}

export default useSetBgImage
