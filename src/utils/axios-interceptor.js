import axios from  'axios';
import setting from '../config/settings'

const token = localStorage.getItem('auth') ? localStorage.getItem('auth') : '';

const axiosInstance = axios.create({
//baseURL : setting.API_URL,
headers : {
    
    Authorization : `Bearer ${token}`
},
});

export default axiosInstance;