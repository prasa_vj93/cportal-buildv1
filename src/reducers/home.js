import * as actionTypes from '../constant/actionTypes'; 

// const initialState = {
// user : [],
// users : {},
// loading : true,
// path : null

// };


const  reducer  = (state = {},action ) => {

     switch(action.type) {
      
     case actionTypes.INIT_LOADER :
        return Object.assign({}, state, {
            loader: true
        });
     case actionTypes.REMOVE_LOADER :
        delete state.loader;
        return Object.assign({}, state);       
     case actionTypes.AUTHENTICATE :
         return Object.assign({}, state, {
             users: action.user
           }); 
     case actionTypes.LOGIN_URL :
         return Object.assign({},state,{
            path: action.pathUrl 
         })      
      default :
       return state ;

   }
};

export default reducer;