import * as actionTypes from '../constant/actionTypes';

const initialState = {
dash : [],


};


const  reducer  = (state = initialState,action ) => {

     switch(action.type) {
      
     case actionTypes.INIT_LOADER :
        return Object.assign({}, state, {
            loader: true
        });
     case actionTypes.REMOVE_LOADER :
        delete state.loader;
        return Object.assign({}, state);       
     case actionTypes.AUTHENTICATE :
         return Object.assign({}, state, {
             user: action.user
           }); 
      default :
       return state ;

   }
};

export default reducer;