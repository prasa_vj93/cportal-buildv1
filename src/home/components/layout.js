import React from 'react';
import Header from './header';
import Footer from './footer';
import AppRouter from './appRouter';


function Layout() {
return(
    <React.Fragment>
         <Header />
            <AppRouter/>
         <Footer/>
</React.Fragment>
   
    

);
}

export default Layout;

