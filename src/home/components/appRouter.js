import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Login from '../../login';
import ForgotPassword from '../../login/components/forgotPassword';
import ForgotUser from '../../login/components/forgotUser';
import Dashboard from './dashboard';




function AppRouter()
{
    return(
        <Router>
            <Switch>
            <Route path="/" exact component={ Login } />
            <Route path="/forgotPassword"  component={ ForgotPassword } />
            <Route path="/forgotUser" component={ ForgotUser } />
            <Route path="/dashboard" component={ Dashboard } />


            </Switch>
        </Router>
    );
}

export default connect()(AppRouter);