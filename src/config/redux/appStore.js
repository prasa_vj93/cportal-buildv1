import { createStore,applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import rootReducer from './rootReducer';
import thunk from 'redux-thunk';



const middlewares = [thunk];

const enhancer = composeWithDevTools(applyMiddleware(...middlewares));

export const store = createStore(
rootReducer,    
enhancer
);

export const persistor=persistStore(store);

export default { store,persistor };