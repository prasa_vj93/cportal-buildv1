import React from "react";
import FloatingMenu from "./FloatingMenu";

const Footer = () => {
  return (
    <>
      <footer>
      <div className="container">
        <div className="footer"></div>
      </div>
        {/* <div className="footer-bottom-wrapper brand-dark-grey top-white divider">
        <div className="container-fluid">
          <div className="outerpadding">
            <div className="row">
              <div className="col-12">
                <div className="upper-div">
                  <div className="footer-container">
                    <div className="footer-container__logo">
                      <div className="footer-logo">
                        <a itemprop="url" href="/">
                          <img
                            src="images/footer-logo.png"
                            alt="Footer Logo"
                            width="217"
                            height="50"
                            disablewebedit="False"
                          />
                        </a>
                      </div>
                    </div>
                    <div className="footer-container__links">
                      <div className="lcol">
                        <ul>
                          <li>
                            <a
                              href="/about-us/our-solutions"
                              target=""
                              title="Our Solutions"
                            >
                              Our Solutions
                            </a>
                          </li>
                          <li>
                            <a
                              href="/investor-relations"
                              target=""
                              title="Investor Relations"
                            >
                              Investor Relations
                            </a>
                          </li>
                          <li>
                            <a
                              href="/press-and-media"
                              target=""
                              title="Press and Media"
                            >
                              Press and Media
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="lcol">
                        <ul>
                          <li>
                            <a
                              href="/about-us/our-businesses"
                              target=""
                              title="Our Businesses"
                            >
                              Our Businesses
                            </a>
                          </li>
                          <li>
                            <a
                              href="/about-us/financial-achievements"
                              target=""
                              title="Our Achievements"
                            >
                              Our Achievements
                            </a>
                          </li>

                          <li>
                            <a href="#" target="" title="CSR">
                              CSR
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="lcol footerLinkWrap">
                        <ul className="footer-link">
                          <li>
                            <a href="" target="" title="Locat Us">
                              Locat Us
                            </a>
                          </li>
                          <li>
                            <a
                              href="/privacy-policy"
                              target=""
                              title="Privacy Policy"
                            >
                              Privacy Policy
                            </a>
                          </li>
                          <li>
                            <a
                              href="/terms-and-conditions"
                              target=""
                              title="Terms and Conditions"
                            >
                              Terms and Conditions
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="lower-div">
                  <div className="copyright">
                    <p className="caption">
                      <span>&copy; 2019, Aditya Birla Capital Ltd.</span>
                      <span>All Rights Reserved.</span>
                    </p>
                  </div>
                  <div className="socialicons">
                    <div className="contacttext">
                      <p className="caption">Call us toll free:</p>
                      <span className="icon-phone icon"></span>
                      <p className="caption">
                        <a href="tel:18002707000">1800 270 7000</a>
                      </p>
                    </div>
                    <ul>
                      <li>
                        <a href="#" target="_blank">
                          <span className="icon-facebook icon"></span>
                        </a>
                      </li>

                      <li className="">
                        <a href="#" target="_blank">
                          <span className="icon-instagram icon"></span>
                        </a>
                      </li>
                      <li className="">
                        <a href="#" target="_blank">
                          <span className="icon-linkedIn icon"></span>
                        </a>
                      </li>
                      <li className="">
                        <a href="javascript:;" target="_blank">
                          <span className="icon-twitter icon"></span>
                        </a>
                      </li>
                      <li className="">
                        <a href="#" target="_blank">
                          <span className="icon-youtube icon"></span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      </footer>
    </>
  );
};

export default Footer;
