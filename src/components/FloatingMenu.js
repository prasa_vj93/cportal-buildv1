import React,{useState} from 'react'
import useGetPositions from '../hooks/useGetPositions';
import { NavLink } from 'react-router-dom';

function FloatingMenu() {
    const [mfloatingBtnState, setMfloatingBtnState] = useState(false);
    const [floatingBtnPosition] = useGetPositions('container--refContainer');
    const mFloatingMenuStateHandler=()=>{
        if(mfloatingBtnState){
            setMfloatingBtnState(false)
        }else{
            setMfloatingBtnState(true)
        }
    }
    return (
        <>
            <div className={`floatingBtn ${(mfloatingBtnState)?'floatingBtn--active':''}`} style={{'right':(floatingBtnPosition.right+30)+'px'}}>
                <ul className="floatingBtn__list mobile">
                    <li className="floatingBtn__item">
                        <button className="floatingBtn__link" title="chat">
                            <span className="floatingBtn__text">chat</span>
                            <span className="floatingBtn__round icon icon-chat"></span>
                        </button>
                    </li>
                    <li className="floatingBtn__item">
                        <button className="floatingBtn__link" title="Branch Locator">
                            <span className="floatingBtn__text">Branch Locator</span>
                            <span className="floatingBtn__round icon icon-branch-locator"></span>
                        </button>
                    </li>
                    <li className="floatingBtn__item">
                        <button className="floatingBtn__link" title="Call">
                            <span className="floatingBtn__text">Call</span>
                            <span className="floatingBtn__round icon icon-call"></span>
                        </button>
                    </li>
                </ul>
                <button title="Icon Cancel" onClick={mFloatingMenuStateHandler} className="mobile floatingBtn__round floatingBtn--btn-icon">
                    <span className="mobile icon icon-cancel floatingBtn__close"></span>
                </button>
                <button title="Icon Chat" onClick={mFloatingMenuStateHandler} className="desktop floatingBtn__round floatingBtn--btn-icon">
                    <span className="desktop icon icon-chat"></span>
                </button>
            </div>
            <span className={`overlay ${(mfloatingBtnState)?'active':''}`}></span>
        </>
    )
}

export default FloatingMenu
