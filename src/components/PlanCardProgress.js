import React from 'react'

function PlanCardProgress(props) {
    return (
        <div className="planCard__goal">
            <label className="planCard__lbl">Goal achieved</label>
            <span className="planCard__status-bar">
                <span className="planCard__total"></span>
                <span className="planCard__achive" style={{'width':props.progress+'%'}}></span>
            </span>
            <span className="planCard__status">
                <span>{props.progress}</span>
                <span>%</span>
            </span>
        </div>
    )
}

export default PlanCardProgress
