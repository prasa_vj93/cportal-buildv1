import React,{useState,useEffect} from "react";
import useGetPositions from '../hooks/useGetPositions';
import { isMobile } from "react-device-detect";
import { NavLink } from "react-router-dom";
const SideNav = (props) => {
  const [navState,setNavState]= useState(true);
  const [hvrClass,setHvrClass] = useState('') ;
  const [docHeight,setDocHeight] = useState(0)
  const [sideNavPosition] = useGetPositions('container--refContainer');
  let body = document.body,
            html = document.documentElement

  const navStateHandler = (e)=>{
    e.preventDefault();
    setNavState(false);
    setTimeout(()=>{
      setHvrClass('sideNav--hvr')
    },1000)
  }
  useEffect(()=>{
    if(!isMobile){
      setDocHeight(Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ));
    }else{
      setDocHeight(0)
    }
  },[])
  return (
    <>
      <div className={`sideNavOuter ${props.docClass}`} style={{'left':`${(props.docClass)?sideNavPosition.left+'px':''}`}}>
        <div className={`sideNav ${(navState)?'sideNav--active':''} ${(props.openStateMobile)?'sideNav--open':''} ${hvrClass} `} style={{'height':`${(docHeight)>0?docHeight+'px':'auto'}`}}>
          <button className="btn sideNav__openClose" onClick={(e)=>{navStateHandler(e)}} >
            <span className="sideNav__close">
              <span className="lineBreak">View</span>
              <span className="lineBreak">Menu</span>
            </span>
            <span className="sideNav__open">
              <span className="sideNav__text">Close Menu</span>
              <span className="icon icon-cancel"></span>
            </span>
          </button>
          <div className="sideNav__navList">
            <ul className="sideNav__list">
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="View Profile">
                  <span className="icon user-name">KR</span>
                  <span className="sideNav__text">View Profile</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Dashboard">
                  <span className="icon icon-dashboard"></span>
                  <span className="sideNav__text">Dashboard</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Policy Details">
                  <span className="icon icon-policy-details"></span>
                  <span className="sideNav__text">Policy Details</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Pay premium">
                  <span className="icon icon-pay"></span>
                  <span className="sideNav__text">Pay premium</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Downloads">
                  <span className="icon icon-download"></span>
                  <span className="sideNav__text">Downloads</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Service Requests">
                  <span className="icon icon-service-request"></span>
                  <span className="sideNav__text">Service Requests</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Status tracker">
                  <span className="icon icon-status-tracker"></span>
                  <span className="sideNav__text">Status tracker</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="FAQs">
                  <span className="icon icon-faq"></span>
                  <span className="sideNav__text">FAQs</span>
                </NavLink>
              </li>
              <li className="sideNav__item">
                <NavLink className="sideNav__link" to="" title="Terms and Conditions">
                  <span className="icon icon-tc"></span>
                  <span className="sideNav__text">Terms and Conditions</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

export default SideNav;
