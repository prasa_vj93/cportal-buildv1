import React,{useEffect,useRef,useState} from "react";
import { NavLink } from "react-router-dom";

const PlanCard3 = () => {
  const node = useRef();
  const [kebabMenuState,setKebabMenuState] = useState(false)
  const handleClickOutside = (e)=>{

    if (node.current.contains(e.target)) {
      // inside click

      return;
    }

    setKebabMenuState(false);
    // outside click 
  }
  useEffect(() => {
    // add when mounted
    document.addEventListener("click", handleClickOutside);
    // return function to be called when unmounted
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);
  const setKebabMenuStateHandler=()=>{
    setKebabMenuState(!kebabMenuState); 
  }
  return (
    <>
      <div  className="planCard">
        <div className="planCard__head">
          <ul className="planCard__list">
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Policy No.</label>
                <span className="lblText__text">726183618</span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Policy Type</label>
                <span className="lblText__text">
                  Unit Life Insurance Policy
                </span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Plan Type</label>
                <span className="lblText__text">
                  ABSLI Weath Assured Plan
                </span>
              </div>
            </li>
          </ul>

          <span className="tag tag--active">Active</span>

          <div  className={`planCard__dote-menu ${(kebabMenuState)?'active':''}`}>
            <span ref={node} className="icon icon-kebab-menu" onClick={setKebabMenuStateHandler}></span>
            <ul  className="planCard__menu">
              <li className="planCard__item">
                <NavLink className="planCard__link" to="" title="Download statement">
                  Download statement
                </NavLink>
              </li>
              <li className="planCard__item">
                <NavLink className="planCard__link" to="" title="Switch funds">
                  Switch funds
                </NavLink>
              </li>
              <li className="planCard__item">
                <NavLink className="planCard__link" to="" title="Change registered address">
                  Change registered address
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
        <div className="planCard__cont">

          <ul className="planCard__list">
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Purpose of Insurance</label>
                <span className="lblText__text">Child Protection</span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Sum Assured</label>
                <span className="lblText__text">&#8377; 50,00,000</span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Total Premium</label>
                <span className="lblText__text">&#8377; 60,000</span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Fund Value</label>
                <span className="lblText__text">&#8377; 25,000</span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">Policy Term</label>
                <span className="lblText__text">10 years</span>
              </div>
            </li>
            <li className="planCard__item">
              <div className="lblText">
                <label className="lblText__lbl">NAV</label>
                <span className="lblText__text">&#8377; 13.70</span>
              </div>
            </li>
          </ul>

          <div className="planCard__actBtn actBtn">
            <button className="btn btn--outline" type="button">
              Details
            </button>
            <button className="btn" type="button">
              Pay Premium
            </button>
          </div>
        </div>
        <div className="planCard__upgrade">
          <h3 className="planCard__title">
            <span className="icon icon-star"></span>
            <span>Congratulations!</span>
          </h3>
          <p className="planCard__para">
            Special offer for you! Get a cover of Cr for premium of
            &#8377;10,000
          </p>
          <button className="planCard__btn btn btn--link" type="button">
            Upgrade Now
          </button>
        </div>
      </div>
    </>
  );
};

export default PlanCard3;
