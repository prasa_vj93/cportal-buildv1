import React, { useState } from 'react'
import Snackbar from '@material-ui/core/Snackbar';

function Toaster(props) {
    const [open, setOpen] = useState(true);
    const [messageInfo, setMessageInfo] = useState(undefined);
 

    const handleClose = (event, reason) => {
        setOpen(false);
    };

    const handleExited = () => {
        setMessageInfo(undefined);
    };
    return (
        <>
            <Snackbar
                key={messageInfo ? messageInfo.key : undefined}
                anchorOrigin={{
                    vertical: (props.vAlign) ? props.vAlign : 'top',
                    horizontal: (props.hAlign) ? props.hAlign : 'right',
                }}
                className="toaster"
                open={open}
                autoHideDuration={30000}
                onClose={handleClose}
                onExited={handleExited}
                message={
                    <>
                        <label className="toaster__Title">{props.toastType}</label>
                        <p className="toaster__Message">{props.toastMessage}</p>
                    </>
                }
            />
                
        </>
    )
}

export default Toaster
