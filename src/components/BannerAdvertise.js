import React from 'react'
import { NavLink } from 'react-router-dom'

function BannerAdvertise() {
    return (
        <div className="advertise advertise--typGradient">
            <div className="advertise__img setBgSrc">
                <picture >
                    <source
                        media="(min-width: 1021px)"
                        srcSet="/assets/images/advertise-bg.jpg"
                    />
                    <source
                        media="(min-width: 760px) and (max-width: 1020px)"
                        srcSet="/assets/images/advertise-bg-m.jpg, /assets/images/advertise-bg-m.jpg 2x"
                    />
                    <source
                        media="(max-width: 759px)"
                        srcSet="/assets/images/advertise-bg-m.jpg, /assets/images/advertise-bg-m.jpg 2x"
                    />
                    <img
                        className="getBgSrc"
                        src="/assets/images/advertise-bg.jpg"
                        alt="ABCL - Blog"
                        title="Advertisement Banner"
                        alt="Advertisement Banner"
                    />
                </picture>
            </div>

            <div className="advertise__desc">
                <h3 className="advertise__title">
                    <span className="advertise__big">
                        83<em>%</em>
                    </span>
                      People your age have an ULIP Policy
                    </h3>
                <div className="advertise__mob">
                    <h4 className="advertise__subtitle">
                        Buy additional cover of &#8377;1.5 Cr @ just
                        &#8377;1,500 per month
                      </h4>
                    <div className="actBtn advertise__actBtn">
                        <NavLink to="" className="btn" title="Other Plans">
                            Other Plans
                        </NavLink>
                        <NavLink to="" className="btn btn--white" title="Know More">
                            Know More
                        </NavLink>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default BannerAdvertise
