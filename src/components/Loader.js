import React from 'react'

const Loader = (props) => {
    return (
        <>
            <span className={`loader ${(!props.show)?'loader--hide':''} `}></span>
        </>
    )
}

export default Loader;
