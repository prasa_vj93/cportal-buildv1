import React, { useState, useEffect } from "react";
import DateFnsUtils from "@date-io/date-fns";
import Loader from "../../home/containers/loader";
import RightBanner from './rightBanner';
import {
  MuiPickersUtilsProvider,
  //KeyboardTimePicker,
  //DatePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import { NavLink } from "react-router-dom";

const ForgotPassword = () => {
  const [loader,setLoader] = useState(true);
  const [selectedDate, setSelectedDate] = useState(null);
  useEffect(()=>{
    setLoader(false);
  },[])
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  return (
    <>
      
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to="" title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Forgot Password</h1>
                <label className="loginJourney__lbl">
                  Enter your information below to continue
                </label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="formGroup formGroup--error">
                    <label className="lbl">One ABC ID</label>
                    <input
                      type="text"
                      className="formControl"
                      placeholder="Enter hrer"
                    />
                    <span className="error">
                      Please check your One ABC ID and try again
                    </span>
                  </div>
                  <div className="formGroup">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        autoOk
                        disableFuture
                        inputVariant="standard"
                        openTo="year"
                        format="dd/MM/yyyy"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        label="Policy Owner Date of Birth (DD/MM/YYYY)"
                        placeholder="DD/MM/YYYY"
                        views={["year", "month", "date"]}
                        value={selectedDate}
                        onChange={handleDateChange}
                        keyboardIcon={''}
                        helperText={''}
                      />
                    </MuiPickersUtilsProvider>
                    <span className="error">
                      Please check the Policy No./Application No. and try again
                    </span>
                  </div>
                  <div className="lytLogin__actBtn actBtn">
                    <button className="btn" type="button">
                      Send OTP
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <RightBanner />
        </div>
      </div>
      
      <Loader show={loader} />
    </>
  );
};

export default ForgotPassword;
