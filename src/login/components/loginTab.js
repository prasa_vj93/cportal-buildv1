import React, { useState,useEffect } from "react";
import { NavLink } from "react-router-dom";
import Loader from "../../home/containers/loader";
import RightBanner from './rightBanner';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

const LoginTab = props => {
  const history=useHistory();
  const [loader,setLoader] = useState(true);
  const [tab, setTab] = useState("tab2");
  const [pwdShowState, setPwdShowState] = useState(false);
  const [userId,setUserId]=useState(props.abcId);
  const [password,setPassword] =useState('');
  const [passwordError,setPassworderror]=useState(false);
  const [userError,setUserError]=useState(false);

  useEffect(()=>{
    setLoader(false);
  },[])

  const goBack = event =>{
      event.preventDefault();
      props.goBackfn();
  }

  const handleuserSubmit = async(event) => {
    event.preventDefault();  
    setLoader(true);
    const data ={ userId : userId , password :password };
    //console.log(data);
    const response = await axios.get("https://jsonplaceholder.typicode.com/posts/1");
    if(response.data && response.data.id===1)
    {
      console.log(response.data.id)
      setLoader(false);
      history.push('/dashboard');
      
    }

  }

  const setTabHandler = (e, param) => {
    e.preventDefault();
    switch (param) {
      case 'tab1':
        setTab('tab1');
        break;
      case 'tab2':
        setTab('tab2');
        break;
      default :
       setTab('tab1');  
    }
  }
  const pwdShowStateHandler = () => {
    setPwdShowState(!pwdShowState);
  }
  return (
    <>
     
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <NavLink className="btnBack" to=""  onClick={ goBack.bind(this)} title="Go Back">
              <span className="icon icon-arrow-left"></span>
              <span className="text">Go Back</span>
            </NavLink>
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Login</h1>
                <label className="loginJourney__lbl">Enter your information below to continue</label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="tab">
                    <ul className="tab__nav">
                      <li className={`tab__navBtn ${(tab === 'tab1') ? 'tab__navBtn--active' : ''}`}>
                        <NavLink to="#tab1" title="Login with password" onClick={(e) => { setTabHandler(e, 'tab1') }}>Login with password</NavLink>
                      </li>
                      <li className={`tab__navBtn ${(tab === 'tab2') ? 'tab__navBtn--active' : ''}`}>
                        <NavLink to="#tab2" title="Login with OTP" onClick={(e) => { setTabHandler(e, 'tab2') }}>Login with OTP</NavLink>
                      </li>
                    </ul>
                    <div className="tab__content">
                      <div id="tab1" className={`tab__pane ${(tab === 'tab1') ? 'tab__pane--active' : ''}`}>
                        <div className="inputGroup">
                          <label className="lbl">User ID</label>
                          <input
                            type="text"
                            className="formControl"
                            placeholder="Enter your User ID"
                            value={ userId }
                            onChange={ event =>{
                              setUserId(event.target.value);
                            } }
                          />
                          <div className="inputGroupAppend">
                            <NavLink to="" className="btn btn--link" title="Forgot User ID?">Forgot User ID?</NavLink>
                          </div>
                          <span className={ userError ? "error-active" : "error"}>
                            We did not find this ID in our database. Please
                            check User ID and enter again
                                </span>
                        </div>
                        <div className="inputGroup inputGroup--pass">
                          <label className="lbl">Password</label>
                          <div className="inputGroup__addon">
                            <input
                              type={`${(pwdShowState) ? 'text' : 'password'}`}
                              className="formControl"
                              placeholder="Enter Password"
                              value ={ password }
                              onChange={event =>{   
                                setPassword(event.target.value)
                              }
                            }
                            />

                            <button className="btn" onClick={pwdShowStateHandler} type="button">
                              <span className={`icon ${(pwdShowState) ? 'icon-no-view' : 'icon-view'}`}></span>
                            </button>
                          </div>
                          <div className="inputGroupAppend">
                            <NavLink to="/forgotPassword" className="btn btn--link" title="Forgot password?">Forgot password?</NavLink>
                          </div>
                          <span className={ passwordError ? "error-active" : "error"}>
                            Your password in incorrect. Please try again.
                          </span>
                        </div>
                        <div className="lytLogin__actBtn actBtn">
                          <button className="btn btn--link" type="button">
                            Register now
                                </button>
                          <button className="btn" type="button" onClick={ handleuserSubmit }>
                            Submit
                                </button>
                        </div>
                      </div>
                      <div id="tab2" className={`tab__pane ${(tab === 'tab2') ? 'tab__pane--active' : ''}`}>
                        <div className="inputGroup">
                          <label className="lbl">One ABC ID</label>
                          <input
                            type="text"
                            className="formControl"
                            placeholder="Enter your User ID"
                          />
                          <div className="inputGroupAppend">
                            <NavLink to="/forgotUser" className="btn btn--link" title="Forgot User ID?">Forgot User ID?</NavLink>
                          </div>
                          <span className="error">
                            We did not find this ID in our database. Please
                            check User ID and enter again
                                </span>
                        </div>
                        <div className="lytLogin__actBtn actBtn">
                          <button className="btn" type="button">
                            Send OTP
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
              <RightBanner />
        </div>
      </div>
      
      <Loader show={loader} />
    </>
  );
};

export default LoginTab;
