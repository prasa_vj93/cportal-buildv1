import React,{ useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import RightBanner from './rightBanner';
import LoginModal from '../components/loginModel';
import Loader from '../../home/containers/loader';

const LoginUserId = props => {
  
  const [loginModalState,setLoginModalState] = useState(false);
  const [pwdShowState,setPwdShowState] = useState(false);
  const [ enableSubmitButton,setSubmitButton] = useState(true);
  const [usersErrorMessage,setusersErrorMessage ]=useState('');
  const [ entereduserId , setUserId] = useState('');
  const [ enteredPassword , setPassword ]= useState('');
  const [userErrorMessage ,setUserMessage]  = useState(false); 
  const [enablePasswordInput,setPasswordInput] = useState(false);
  const [passwordError,setpasswordError]=useState(false);
  const [loader,setLoader] = useState(true);
  useEffect(()=>{
    setLoader(false);
  },[])

  
    const chkUserId = event => {
       event.preventDefault();
         if(!entereduserId)
       {
        setUserMessage(true);
        setusersErrorMessage('Please enter UserId');
       }
      else{
         console.log('prasad');
        setusersErrorMessage(null);
        setPasswordInput(true);
      }
   }
   const handleChange = event => {
       if(entereduserId.length===0)
       {
        setUserMessage(true);
        setusersErrorMessage('Please enter UserId')
        setpasswordError(false);
       }else if(enteredPassword.length===0)
       {
         //console.log('prasad');
        setUserMessage(false);
        setpasswordError(true);
        setusersErrorMessage('Please enter Password')
       }else{
        setUserMessage(false);
        setpasswordError(false);
        setusersErrorMessage(null);
       }
   } 
   
   const submitData = event => {
    event.preventDefault();
    const data = {userId : entereduserId , password : enteredPassword };
    props.handleLogin(data);

   }


  const closeLoginModal = ()=>{
    setLoginModalState(false);
  }
  const pwdShowStateHandler = ()=>{
      
    setPwdShowState(!pwdShowState);
  }
  return (
    <>
      
      <div className="container">
        <div className="lytLogin">
          <div className="lytLogin__lhs">
            <div className="loginJourney">
              <div className="loginJourney__head">
                <h1 className="loginJourney__title">Login</h1>
                <label className="loginJourney__lbl">
                  Enter your information below to continue
                </label>
              </div>
              <div className="loginJourney__cont card">
                <form>
                  <div className="inputGroup">
                    <label className="lbl">User ID</label>
                    <input
                      type="text"
                      className="formControl"
                      placeholder="Enter your User ID"
                      name="userID"
                      value ={ entereduserId }
                      onChange = { event => {
                           setUserId(event.target.value) 
                        } }
                      onKeyUp={handleChange}
                      
                    />
                    <div className="inputGroupAppend">
                      <NavLink className="btn btn--link" to="/forgotUser" title="Forgot User ID?">Forgot User ID?</NavLink>
                    </div>
                    <span className={userErrorMessage ? 'error-active' : 'error'}>
                      { usersErrorMessage }
                    </span>
                    
                  </div>
                  { enablePasswordInput && <div className="inputGroup inputGroup--pass">
                    <label className="lbl">Password</label>
                    <div className="inputGroup__addon">
                      <input
                        type={`${(pwdShowState)?'text':'password'}`}
                        className="formControl"
                        placeholder="Enter Password"
                        name ="password"
                        value ={ enteredPassword } 
                        onChange = { event => { setPassword(event.target.value) }}
                        required
                        onKeyUp={handleChange}
                      />
                      <button className="btn" onClick={pwdShowStateHandler} type="button">
                        <span className={`icon ${(pwdShowState)?'icon-no-view':'icon-view'}`}></span>
                      </button>
                    </div>
                    <div className="inputGroupAppend">
                      <NavLink className="btn btn--link" to="/forgotPassword" title="Forgot password?">Forgot password?</NavLink>
                    </div>
                    <span className={passwordError ? 'error-active' : 'error'}>
                      { usersErrorMessage }
                    </span>
                    {/* <span className={emptyPassword ? 'error-active' : 'error'}>
                      Please Enter the password to continue
                    </span> */}
                    </div> }
             <div className="lytLogin__actBtn actBtn">
                    <button className="btn btn--link" type="button">
                      Register now
                    </button>
                    {/* <button className="btn" type="button">
                      Submit
                    </button> */}
                     { !enablePasswordInput ?  
                     <button className='btn' type="button" onClick={ chkUserId } >
                      Check UserID
                     </button>  : 
                     <button className='btn' type="button" onClick= { submitData } >
                     Submit
                    </button>
                     
                     }


                  </div>
                </form>
              </div>
            </div>
          </div>
          <RightBanner />
          <LoginModal loginModalState={ loginModalState } closeLoginModal={ closeLoginModal } />
          <Loader show={loader} />
        </div>
      </div>
      
      
    </>
  );
};

export default LoginUserId;
