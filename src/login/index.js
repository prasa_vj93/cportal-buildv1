import React,{ useState } from 'react';
import LoginTab from './components/loginTab';
import LoginUserid from './components/loginUserid';
import { connect } from 'react-redux';
import { authenticate } from '../actions/auth';
import   * as services  from '../utils/validationServices';
import { useHistory } from 'react-router-dom';




const Login = props => {


const history=useHistory();
const [loginUserid,setLoginUserid] = useState(true);     
const [abcId,setabcId] = useState('');

const buildLoginMenu = () => {
if(loginUserid)
{
    //console.log('prasad');
    return <LoginUserid  
            //handleUserid={ chkUserId }  
            handleLogin ={ handleLogin } 
            />
}else{
    return <LoginTab 
            goBackfn={ handleBack }
            abcId = { abcId } />
}
}

const handleBack = () => {

  setLoginUserid(true);
 // setLoginTab(false);

}


const handleLogin = data => {
 //console.log(data);
 setLoginUserid(false);
 //setLoginTab(true);
 setabcId('Temp');

} 

return(
    <>
 { buildLoginMenu() }
 
    </>

);
}

const mapStateToProps = state => {
    return {
      authenticate : state.home
    }
  }
  
  
  const mapDispatchToProps = dispatch => {
  return {
    authenticate : data => dispatch(authenticate(data))
  
    };
  };  

export default connect(mapStateToProps,mapDispatchToProps)(Login);